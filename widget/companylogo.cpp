/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Implementation of the CompanyLogo class
** File name: companylogo.cpp
**
****************************************************************/

#include <QPainter>
#include <QString>

#include "companylogo.h"
#include "errormessage.h"

CompanyLogo::CompanyLogo(QWidget* parent, bool white)
    : QWidget(parent)
{
    // Load the company logo
    loadLogo(white);
    // Set widget size to image size (fixed)
    setFixedSize(logo.width(), logo.height());
}

void CompanyLogo::paintEvent([[maybe_unused]] QPaintEvent* revent)
{
    QPainter painter(this);
    // Draw the image in the left top corner of the widget
    painter.drawImage(0, 0, logo);
}

void CompanyLogo::loadLogo(bool white)
{
    QString logoColor;
    // Set the color as part of the file name
    if (white) {
        logoColor = "white";
    } else {
        logoColor = "orange";
    }
    // Load the image
    if (!logo.load(":/img/logo_" + logoColor + ".png")) {
        // Error message
        ErrorMessage* errorMessage = new ErrorMessage(this);
        errorMessage->showMessage(Error::logo_pic, ErrorMessage::Type::Warning,
            ErrorMessage::Cancel::Operation);
    }
}
