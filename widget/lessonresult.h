/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the LessonResult class
** File name: lessonresult.h
**
****************************************************************/

#include <QChar>
#include <QList>
#include <QPushButton>
#include <QSqlQuery>
#include <QWidget>

#ifndef LESSONRESULT_H
#define LESSONRESULT_H

//! The LessonTableSql class provides a table widget with lessons.
/*!
        @author Tom Thielicke, s712715
        @version 0.0.2
        @date 16.06.2006
*/
class LessonResult : public QWidget {
    Q_OBJECT

public:
    LessonResult(int row, QList<QChar> charlist, QList<int> mistakelist,
        QWidget* parent = nullptr);

private slots:
    void createPrintOutput();

private:
    void readData();
    void createOutput();
    QPushButton* buttonPrintLesson;
    QList<QChar> charList;
    QList<int> mistakeList;
    int lessonRow;

    QString settingsDuration;
    QString settingsError;
    QString settingsHelp;

    QString lessonName;
    QString lessonTimestamp;
    QString lessonTimeLen;
    QString lessonTokenLen;
    int lessonErrorNum;
    QString lessonCpm;
    QString lessonGrade;
    QString lessonRate;
};

#endif // LESSONRESULT_H
