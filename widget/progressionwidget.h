/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the ProgressionWidget class
** File name: progressionwidget.h
**
****************************************************************/

#ifndef PROGRESSIONWIDGET_H
#define PROGRESSIONWIDGET_H

#include <QComboBox>
#include <QGraphicsSceneMouseEvent>
#include <QImage>
#include <QLabel>
#include <QList>
#include <QPaintEvent>
#include <QString>
#include <QWidget>

#include "def/defines.h"

//! The ProgressionWidget class provides a progression chart.
/*!
        @author Tom Thielicke, s712715
        @version 0.1.6
        @date 16.06.2006
*/
class ProgressionWidget : public QWidget {
    // Necessary to create own signals, slots and connections
    Q_OBJECT

public:
    ProgressionWidget(QWidget* parent = 0);

signals:

private slots:
    void changeFilter(int rowindex);
    void changeOrder(int rowindex);

protected:
    //! Paintevent, draws current view of the chart.
    void paintEvent(QPaintEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

private:
    void getChartValues();
    void drawGrid();
    void drawGraph();
    void drawNothing();
    void drawTooltip(QPainter* painter, double x, double y, QString message);

    QList<QString> lessonsNumbers;
    QList<QString> lessonsTimestamps;
    QList<QString> lessonsNames;
    QList<QString> lessonsAxis;
    QList<int> lessonsGrades;
    QList<int> lessonsCpms;
    QList<int> lessonsType;
    QList<double> lessonsX;
    QList<double> lessonsY;
    QLabel* labelFilter;
    QComboBox* comboFilter;
    QLabel* labelOrder;
    QComboBox* comboOrder;
    int lessonCounter;
    int lessonGradeMax;
    int lessonAv;
    QString whereClausel;
    QString orderClausel;
    QString xAxis;
    int xAxisColumn;
    int lessonSelected;
};

#endif // PROGRESSIONWIDGET_H
